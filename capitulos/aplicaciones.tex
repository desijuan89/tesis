En este capítulo veremos algunas aplicaciones de las matrices de representación. Estas matrices pueden ser usadas para calcular preimágenes de puntos de $\PP_k^3$, para dar una noción de distancia a la superficie (o curva) y para calcular proyecciones ortogonales. Para más información puede consultarse \cite{Buse14}. A lo largo de la tesis trabajamos fundamentalmente con superficies, sin embargo los siguientes resultados también sirven para curvas. Al final del capítulo haremos una breve discusión de los temas que se están trabajando actualmente en el área de implicitación de hipersuperficies racionales.

\section{Cálculo de preimágenes}
Si tenemos un punto $\pp = \phi(\qq)$ que vía $\phi$ tiene preimagen única, esta puede ser obtenida calculando el núcleo de la transpuesta de $M_\nu(\phi)$ evaluada en $\pp$, donde $\nu$ es mayor o igual a la regularidad del álgebra Simétrica. Para simplificar tomemos $\nu \geq 2d-2$ que siempre sirve.

Tenemos
\begin{align*}
\Zc_1 &\xra{v_1} \Zc_0 = R[s_0,s_1,s_2] \\
(g_0,g_1,g_2,g_3) &\mapsto g_0x_0 + g_1x_2 + g_2x_2 + g_3x_3 .
\end{align*}
%\begin{eqnarray*}
%(\Zc_1)_\nu &\xra{M(\phi)_\nu}  &(\Zc_0)_\nu \\
%(g_0,g_1,g_2,g_3) &\mapsto &\sum_{i=0}^3 g_i x_i .
%\end{eqnarray*}
Sea $B_1 = \{l_1,\dots,l_r\}$ una base de $(\Zc_1)_\nu$. Recordemos que las $l_i = (g_0^i,g_1^i,g_2^i,g_3^i) \in S_\nu^4$ son syzygies lineales, es decir que cumplen $\sum_{j=0}^3 g^i_j f_j = 0$. Sea $B_2$ una base de $(\Zc_0)_\nu = R[s_0,s_1,s_2]_\nu$ tal que $B_2 \subset k[s_0,s_1,s_2]$.

Tomemos $M(\phi)_\nu$ la matriz de representación en las bases $B_1$ y $B_2$, es decir $M(\phi)_\nu = |v_1|_{B_1B_2}$.
Para cada $0 \leq i \leq r$ llamemos
\begin{align*}
L_i &= {v_1}_\nu (l_i) \\
	&= \sum_{j=0}^3 g_j^i x_j \in R[s_0,s_1,s_2] .
\end{align*}
Escribiendo los elementos de $B_2$ como un vector columna tenemos
\[ M(\phi)_\nu^t \cdot B_2 = (L_1,\dots,L_r) . \]
Luego evaluando en $\pp$ y $\qq$ tenemos
\[ M(\phi)_\nu^t(\pp) \cdot B_2(\qq) = (0,\dots,0) . \]
Por otro lado
\[ \dim_k(\ker M(\phi)_\nu^t(\pp)) = \dim_k(\coker M(\phi)_\nu(\pp)) = 1 . \]
Luego el núcleo de $M(\phi)_\nu^t(\pp)$ está generado por un sólo vector $v = B(\qq)$. Conociendo $v$ podemos despejar $\qq = \phi^{-1}(\pp)$.

\begin{ej} \label{ejPreim}
Volvamos al ejemplo de la esfera. En el capítulo anterior calculamos una matriz de representación $M(\phi)_1$ y vimos que para todo $\pp$ punto distinto de ${(1:0:0:1)}$, la fibra $\phi^{-1}(\pp)$ tiene un sólo elemento. Tomemos por ejemplo ${\pp = {(1:1:0:0)}}$ y $\nu = 2$. Podríamos haber usado la matriz calculada para $\nu = 1$ pero el ejemplo se entiende mejor si usamos valores mayores de $\nu$. Tenemos la matriz de representación transpuesta
\[ M(\phi)_2^t =
\begin{pmatrix}
	0			&	0			&	0			&	x_2		& -x_1		&	0			\\
	0			&	0			&	0			&	0			&	x_2		&	-x_1		\\
	x_2		&	0			&	-x_0+x_3	&	0			&	0			&	0			\\
	0			&	x_2		&	0			&	0			&	-x_0+x_3	&	0			\\
	0			&	0			&	x_2		&	0			&	0			&	-x_0+x_3	\\
	x_1		&	-x_0+x_3	&	0			&	0			&	0			&	0			\\
	0			&	x_1		&	0			&	-x_0+x_3	&	0			&	0			\\
	0			&	0			&	x_1		&	0			&	-x_0+x_3	&	0			\\
	x_0+x_3	&	-x_1		&	-x_2		&	0			&	0			&	0			\\
	0			&	x_0+x_3	&	0			&	-x_1		&	-x_2		&	0			\\
	0			&	0			&	x_0+x_3	&	0			&	-x_1		&	-x_2		\\
\end{pmatrix} . \]
La evaluamos en el punto $\pp$ y calculamos su núcleo
\[ \ker [ M(\phi)_2^t(1:1:0:0) ] = \ <(1,1,0,1,0,0)> . \]
La base $B_2$ de $R[s_0,s_1,s_2]_2$ que usamos para calcular$M(\phi)_2$ es $\{s^2_0,s_0s_1,s_0s_2,s_1^2,s_1s_2,s_2^2\}$.
O sea que
\[ \left\{
\begin{array}{lcl}
s_0^2 & = & 1 \\
s_0s_1 & = & 1 \\
s_0s_2 & = & 0 \\
s_1^2 & = & 1 \\
s_1s_2 & = & 0 \\
s_2^2 & = & 0 \, .
\end{array}
\right. \]
Despejando obtenemos $\phi^{-1}{(1:1:0:0)} = {(1:1:0)}$.
\end{ej}

\section{Noción de distancia a una curva o superficie}
Cuando trabajamos sobre el cuerpo de los números reales, las matrices de representación nos permiten definir una noción de distancia a la superficie (o curva) parametrizada por $\varphi$.
 
Dada $A \in \RR^{m \times r}$ una matriz real con $m \leq r$, una descomposición en valores singulares (SVD) de $A$ es una descomposición de la forma $A = U \Sigma V^t$, donde $U = [u_1,\dots,u_m] \in \RR^{m \times m}$ y $V = [v_1,\dots,v_r] \in \RR^{r \times r}$ son matrices reales ortogonales y $\sigma$ es la matriz
\[ \Sigma =
\left(\begin{array}{ccccccc}
	\sigma_1	&	0	&	 \dots	&	0	&	0	&	\dots	&	0	\\
	0	&  \sigma_2	& \ddots	&	\vdots	&	\vdots	&	&	\vdots	\\
	\vdots	& \ddots	&	\ddots	&	0	&	0	&	\dots	&	0	\\
	0	& \dots	&	0	&	\sigma_m	&	0	&	\dots	&	0
\end{array}\right) . \]
Los elementos de la diagonal de $\Sigma$ son los autovalores (reales) de la matriz simétrica $A^t A$, son no negativos y están ordenados de mayor a menor
\[ \sigma_1 \geq \sigma_2 \geq \dots \geq \sigma_m \geq 0 . \]
Los $\sigma_i$, que también notamos $\sigma_i(A)$, se llaman \emph{valores singulares} de $A$. Los $u_i$ y los $v_i$ se llaman \emph{vectores singulares} a izquierda y a derecha respectivamente. Algo interesante para notar es que si consideramos la imagen de la esfera unitaria bajo la aplicación $x \mapsto Ax$, los valores singulares de $A$ son las longitudes de los semiejes de dicha imagen elipsoidal.
Una propiedad importante de los valores singulares que para nosotros será de utilidad es
\[ \rg(A) = \text{máx}\{i : \sigma_i(A) \neq 0 \} . \] \\

Consideremos una parametrización $\phi\colon \PP_\RR^2 \to \PP_\RR^3$ de una superficie (también vale para curvas). Tomando cartas del dominio y del codominio podemos suponer que dicha parametrización es de la forma
\begin{eqnarray*}
	\phi\colon \AA_\RR^2 & \to & \AA_\RR^3 \\
	(s_1,s_2) & \mapsto & \left( \frac{f_1}{f_0} , \frac{f_2}{f_0} , \frac{f_3}{f_0} \right) (1:s_1:s_2) .
\end{eqnarray*}
\begin{mydef}
Dada $M_\nu = M(\phi)_\nu$ una matriz de representación con $m_\nu$ filas y $r_\nu$ columnas, definimos la \emph{función de evaluación real} de $M_\nu$ como
\begin{eqnarray*}
	\delta_{M_\nu} \colon \AA_\RR^3 & \to & \RR_{\geq 0} \\
	\pp & \mapsto & \prod_{i=1}^{m_\nu} \sigma_i (M_\nu(\pp)) ,
\end{eqnarray*}
donde los $\sigma_i(M_\nu(\pp))$ son los valores singulares de la matriz $M_\nu$ evaluada en $\pp$.
\end{mydef}

Cosas que valen
\[ \delta_{M_\nu}(\pp) = 0 \Leftrightarrow \pp \in \Sc = \overline{\im\phi} . \]
\[ \delta_{M_\nu}(\pp)^2 = \det(M_\nu(\pp)^t \cdot M_\nu(\pp)) . \]
Si $d\colon \AA_\RR^2 \to \RR$ es la distancia euclídea, existen $n_1, n_2 \in \NN$ y $c_1, c_2 \in \RR_{>0}$ tales que
\[ c_1 d(\pp,\Sc)^{n_1} < \delta_{M_\nu}(\pp) < c_2 d(\pp,\Sc)^{n_2} . \]

\section{Proyecciones ortogonales}

\section{Temas que se están trabajando actualmente en el área}
