En este capítulo trabajamos algunos ejemplos. El primero consiste en analizar la aplicación de las técnicas desarrolladas para el caso de una circunferencia. A pesar de tratarse de una curva y no de una superficie la incluimos ya que es un ejemplo sencillo cuya parametrización no tiene puntos base ni puntos singulares y es interesante para comparar con el segundo ejemplo que es una esfera. Si bien ambas están parametrizadas usando la proyección estereográfica, a diferencia de lo que ocurre en el caso de la circunferencia, la parametrización de la esfera sí tiene puntos base. Además en el ejemplo de la esfera la superficie no tiene puntos singulares pero la parametrización sí, y esto salta a la vista cuando calculamos los ideales de Fitting. Los dos siguientes ejemplos son cilindros, uno hecho con la circunferencia y otro con la curva alfa. Difieren en dos aspectos importantes. Por un lado los puntos base del cilindro circular son localmente una intersección completa, por lo que la parametrización cumple las condiciones \eqref{H} (ver Capítulo \ref{capSup}). En cambio el cilindro hecho con la curva alfa no las cumple, tiene un punto base que es localmente una intersección casi completa. Por esta razón en los ideales de Fitting de este último aparece lo que en \cite[Sección 2]{BCJ09} llaman el ``factor extraño''. Por otro lado si bien ambas parametrizaciones tienen puntos múltiples en el caso del cilindro circular, que es una superficie suave, los puntos múltiples son introducidos por la parametrización, mientras que el cilindro hecho con la curva alfa no es suave.

\section{Circunferencia} \label{circ}
Comenzamos analizando el poder explicativo del método para el caso de una posible parametrización de la circunferencia.
Consideremos la siguiente parametrización. Observar que no tiene puntos base, luego cumple \eqref{H}.
\begin{eqnarray*}
	\phi\colon \PP^1_\CC & \to & \PP^2_\CC \\
	( s_0 : s_1 ) & \mapsto & ( s_0^2 + s_1^2 : 2s_0s_1 : s_1^2 - s_0^2 ) .
\end{eqnarray*}
Tomamos $\nu = 2$. Usando Macaulay2 \cite{M2} calculamos la matriz de representación
\[ M(\varphi)_2 =
\begin{pmatrix}
	x_1		&	0			&	x_0+x_2	&	0 \\
	-x_0+x_2	&	x_1		&	-x_1		&	x_0+x_2 \\
	0			&	-x_0+x_2	&	0			&	-x1 \\
\end{pmatrix} .
\]

Una descomposición primaria del ideal de Fitting inicial es
\[ \Fitt_2^0 = (x_0^2 - x_1^2 - x_2^2) \cap (x_2 , x_0x_1 , x_0^2 + x_1^2) . \]
La primera componente es la ecuación implícita de la circunferencia y la segunda está soportada en el origen, es vacía como esquema proyectivo. El próximo ideal de Fitting $\Fitt_2^1$ está soportado en el origen. Sabemos que la circunferencia es no singular, sin embargo podría haber puntos por los que la parametrización pasase más de una vez, es decir, podría haber puntos singulares introducidos por la parametrización. Lo que los ideales de Fitting nos están diciendo es que esto no sucede.

Mirando cartas afines del dominio $U_0 = \PP^2_\CC \setminus V(s_0)$ sin los puntos $\{{(1:i)},{(1:-i)}\}$ y $V_0 = \PP^3_\CC \setminus V(x_0)$, tenemos
\[ \phi|_{U_0 \setminus \{i,-i \} }(s_1) = \left( \frac{2s_1}{s_1^2+1} , \frac{s_1^2-1}{s_1^2+1} \right) , \]
que es inyectiva con inversa a derecha
\[ \phi|_{U_0}^{-1}(x_1,x_2) = \left( \frac{x_1}{1-x_2} \right) . \]
El único punto que va a parar al polo norte es $\phi{(0:1)} = {(1:0:1)}$.
Por otro lado $\phi(1,i) = {(0:2i:-2)}$ y $\phi(1,-i) = {(0:-2i:-2)}$ que son distintos y no caen en $V_0$.
Por lo tanto $\phi$ es inyectiva, por lo que no hay ningún punto de la imagen que sea ``pintado'' más de una vez por la parametrización.

Cuando compactificamos mirando $\AA_\CC^1$ dentro de $\PP_\CC^1$ estamos agregando únicamente un punto, el ${(0:1)}$, cuya imagen vía $\phi$ es el polo norte ${(1:0:1)}$. Tengamos esto en cuenta. A continuación veremos que en la esfera hay algunas diferencias.

\section{Esfera} \label{ejEsfera}
Utilicemos ahora el método para estudiar las propiedades geométricas de la siguiente parametrización de la esfera
\begin{eqnarray*}
	\phi\colon \PP^2_\CC & \dto & \PP^3_\CC \\
	( s_0 : s_1 : s_2 ) & \mapsto & ( s_0^2+s_1^2+s_2^2 : 2s_0s_1 : 2s_0s_2 : s_1^2+s_2^2-s_0^2 ) .
\end{eqnarray*}
El conjunto de puntos base $V(I) = \{ {(0:1:i)}, {(0:1:-i)} \}$ es finito y localmente una intersección completa (l.~c.~i.), luego la parametrización $\phi$ cumple las condiciones \eqref{H}.

En este ejemplo $d = 2$, por lo que deberíamos tomar $\nu \geq \nu_0 = 2$. Sin embargo en este caso podemos tomar $\nu = 1$ (ver \cite[Ejemplo 3]{BBC14}). Obtenemos la siguiente matriz de representación
\[ M(\phi)_1=
\begin{pmatrix}
	0		&	x_2		&	x_1		&	x_0+x_3 \\
	x_2	&	0			&	-x_0+x_3	&	-x_1 \\
	-x_1	&	-x_0+x_3	&	0			&	-x_2 \\ 
\end{pmatrix} , \]
que es mucho más manejable que la matriz correspondiente a $\nu = 2$, que tiene 6 filas y 11 columnas (ver Ejemplo \ref{ejPreim}). A continuación se expone el código en Macaulay2 \cite{M2} utilizado para calcular dicha matriz (y otras, cambiando sus entradas: la parametrización $\phi$ y el entero $\nu$):
{\small
\begin{verbatim}
	>S=QQ[s0,s1,s2];
	>f0=s0^2+s1^2+s2^2; f1=2*s0*s1; f2=2*s0*s2; f3=s1^2+s2^2-s0^2; 
	>d=(degree f0)_0; nu=2*(d-1)-1; -- En general vamos a usar: nu = 2*(d-1).
	>F=matrix{{f0,f1,f2,f3}};
	>Z1=ker F;
	>Z1nu=super basis (nu+d,Z1);
	>R=QQ[x0,x1,x2,x3];
	>A=R[s0,s1,s2];
	>Snu=substitute(basis(nu,S),A);
	>G=matrix{{x0,x1,x2,x3}};
	>ImGnu=G*substitute(Z1nu,A);
	>(m,M)=coefficients(ImGnu,Monomials=>Snu);
	>M -- Ésta es la matriz de representación en grado nu.
\end{verbatim}
}

Una descomposición primaria del ideal generado por los menores de tamaño $3\times 3$ de $M(\phi)_1$, es decir el ideal de Fitting inicial de la parametrización, nos da 
\[ \Fitt^0_1(\phi) = ( x_0^2-x_1^2-x_2^2-x_3^2 ) \cap ( x_1 , x_2 , x_0^2-2x_0x_3 + x_3^2 ) , \]
que corresponde a la ecuación implícita de la esfera y un punto doble embebido ${(1:0:0:1)}$. El ideal de menores de tamaño $2\times 2$ de $M(\phi)_1$ es
\[ \Fitt^1_1(\phi) = ( x_1 , x_2 , x_0 - x_3 ) \cap ( x_0^2 , x_1^2 , x_2^2 , x_3 , x_1x_2 , x_0x_2 , x_0x_1 ) , \]
que corresponde al mismo punto ${(1:0:0:1)}$, ahora con multiplicidad uno, y una componente adicional soportada en el origen. Finalmente $\Fitt^2_1(\phi)$, el ideal de menores de $1\times 1$ de $M(\phi)_1$, está soportado en el origen.

El polo norte $\pp = {(1:0:0:1)}$ es un valor singular de la parametrización $\phi$, no de la esfera en sí. Cuando compactificamos agregamos toda una recta $L = V(s_0)$ al dominio, que vía $\phi$ va a parar al ${(1:0:0:1)}$. Dicho punto tiene una fibra $1$-dimensional, por esta razón aparece en ideal de Fitting $\Fitt^1_1(\phi)$. A diferencia de lo que ocurre con la circunferencia, al compactificar introducimos un punto singular. Otra cosa interesante para observar es que los puntos base de $\phi$, ${(0:1:i)}$ y ${(0:1:-i)}$, pertenecen a la recta del infinito $L$. Sacando $L$ del dominio y $\pp$ del codominio, tenemos que $\phi$ induce un isomorfismo entre $\PP^2_\CC \setminus L = U_0$ y $\PP^3_\CC \setminus \{P\}$.
%Su inversa es
%\[ \phi|_{U_0}^{-1}\colon ( x_1 , x_2 , x_3 ) = ( \frac{x_1}{1-x_3} , \frac{x_2}{1-x_3} ) . \]

\section{Cilindro}
Veamos ahora un ejemplo de una superficie reglada. Tomamos la parametrización de la circunferencia que usamos en \ref{circ} y construimos un cilindro. La parametrización que consideramos es la siguiente
\begin{eqnarray*}
	\phi\colon \PP^2_\CC & \dto & \PP^3_\CC \\
	( s_0 : s_1 : s_2 ) & \mapsto & ( s_0^2+s_1^2 : 2s_0s_1 : s_1^2-s_0^2 : s_0s_2 ) .
\end{eqnarray*}
Hay un único punto base $V(I) = \{{(0:0:1)}\}$, que es localmente una intersección completa, luego se cumplen las condiciones \eqref{H}.
Tomamos $\nu=2$. Calculamos la matriz de representación

\medskip

$M(\phi)_2=$

\noindent\resizebox{\linewidth}{!}{%
$ \left(\begin{array}{ccccccccccc}
	x_1		&	0			&	0			&	x_0+x_2	&	0			&	0			&	0		&	0		&	2x_3		&	0			&	0 \\
	-x_0+x_2	&	x_1		&	0			&	-x_1		&	x_0+x_2	&	0			&	0		&	0		&	0			&	2x_3		&	0 \\
	0			&	0			&	x_1		&	0			&	0			&	x_0+x_2	&	0		&	0		&	-x_0+x_2	&	0			&	2x_3 \\
	0			&	-x_0+x_2	&	0			&	0			&	-x_1		&	0			&	2x_3	&	0		&	0			&	0			&	0 \\
	0			&	0			&	-x_0+x_2	&	0			&	0			&	-x_1		&	-x_1	&	2x_3	&	0			&	-x_0+x_2	&	0 \\
	0			&	0			&	0			&	0			&	0			&	0			&	0		&	-x_1	&	0			&	0			&	-x_0+x_2
\end{array}\right) $}.\\

\medskip

Una descomposición primaria del ideal de menores maximales, el ideal de Fitting inicial de $\phi$, es
\begin{multline*}
\Fitt_2^0 = (x_0^2-x_1^2-x_2^2) \cap (x_1^2,x_3^4,x_0x_3^2-x_2x_3^2,x_0x_1x_3-x_1x_2x_3, \\ x_0^2x_3-2x_0x_2x_3+x_2^2x_3,x_0^2x_1-2x_0x_1x_2+x_1x_2^2, \\ x_0^3-3x_0^2x_2+3x_0x_2^2-x_2^3) \\ \cap ( \text{componente soportada en el origen} ) .
\end{multline*}
La primera componente es la ecuación implícita del cono y la segunda es el punto ${(1:0:1:0)}$ con multiplicidad 3.
El próximo ideal de Fitting es
\begin{multline*}
\Fitt_2^1 = (x_1^2,x_3^2,x_1x_3,x_0x_3-x_2x_3,x_0x_1-x_2x_1,x_0^2-2x_0x_2+x_2^2) \\ \cap ( \text{componente soportada en el origen} ).
\end{multline*}
Vuelve a aparecer el punto ${(1:0:1:0)}$, ahora con multiplicidad 2.
El siguiente es
\[ \Fitt_2^2 = (x_3,x_1,x_0-x_2) \cap ( \text{componente soportada en el origen} ) . \]
Aparece una última vez el punto ${(1:0:1:0)}$, ahora con multiplicidad 1, y una componente soportada en el origen.
Finalmente $\Fitt_2^3$, el ideal de menores de cotamaño 3, está soportado en el origen.

O sea que el único punto singular es $\pp := {(1:0:1:0)}$. A priori no sabemos si su fibra es $1$-dimensional o $0$-dimensional. Usemos los resultados expuestos en la Sección \ref{secAlg} para estudiar la geometría de la fibra $\pi_2^{-1}(\pp)$, más precisamente vemos como es su polinomio de Hilbert. Calculando las matrices de representación para $\nu = 2$ y $\nu = 3$, evaluándolas en $\pp$ y calculando sus rangos obtenemos $\corank(M(\phi)_2(\pp))) = 3$ y $\corank(M(\phi)_3(\pp))) = 4$. Escribiendo $HP_{\pi_2^{-1}(\pp)}(\nu) = a \nu + b $ tenemos
\[ \left\{
\begin{array}{lcl}
2a + b & = & 3 \\
3a + b & = & 4 \, .
\end{array}
\right. \]
Despejando los valores de $a$ y $b$ obtenemos el polinomio de Hilbert de la fibra
\[ HP_{\pi_2^{-1}(\pp)}(\nu) = \nu + 1 . \]
O sea que la fibra de $\pp$ tiene dimensión 1, por lo que es una curva. Más aún tiene grado 1, es una recta. Por último el polinomio de Hilbert también nos dice que no tiene parte finita. Es decir es una recta y ningún punto más. Esto tiene sentido, observando con cuidado la parametrización vemos que $\phi^{-1}(\pp)$ es la recta del infinito $V(s_0)$.

\section{Cilindro sobre la curva alfa} \label{cilCurvAlfa}
Por último veamos un ejemplo que presenta problemas en su lugar base. También se trata de una superficie reglada. Tomamos la curva alfa del Ejemplo \ref{curvas} y construimos el siguiente cilindro
\begin{eqnarray*}
	\phi\colon \PP^2_\CC & \dto & \PP^3_\CC \\
	( s_0 : s_1 : s_2 ) & \mapsto & ( s_0^3 : s_0(s_1^2-s_0^2) : s_1(s_1^2-s_0^2) : s_0^2s_2 ) .
\end{eqnarray*}
El único punto base es $V(I) = \{{(0:0:1)}\}$, que es localmente una intersección casi completa (a.~l.~c.~i.). Luego $\phi$ no cumple las condiciones \eqref{H}, sin embargo calculemos las matrices de representación y veamos como sus ideales de Fitting aún pueden aportarnos información relevante. Tomamos $\nu = 3$. La matriz de representación $M(\phi)_3$ tiene 10 filas y 17 columnas, por eso no la incluimos.

Una descomposición primaria del ideal de Fitting inicial es
\begin{multline*}
\Fitt_3^0 = (x_0) \cap (x_0x_1^2 + x_1^3 + x_0x_2^2) \cap (x_0^7,x_3^6, \\ x_0^2x_1^2x_3^2+x_0x_1^3x_3^2-x_0^2x_2^2x_3^2,x_0^3x_1^2x_3+x_0^2x_1^3x_3-x_0^3x_2^2x_3,x_0^4x_1^2+x_0^3x_1^3-x_0^4x_2^2, \\ x_0^2x_1^4x_3+x_0x_1^5x_3-x_0^2x_1^2x_2^2x_3,x_0^3x_1^4+x_0^2x_1^5-x_0^3x_1^2x_2^2,x_0^2x_1^6+x_0x_1^7-x_0^2x_1^4x_2^2, \\ x_1^9+x_0^6x_1x_2^2+2x_0^2x_1^5x_2^2-3x_0x_1^6x_2^2-x_0^5x_2^4+2x_0^4x_1x_2^4-2x_0^3x_1^2x_2^4+3x_0^2x_1^3x_2^4-x_0^3x_2^6) \\ \cap ( \text{componente soportada en el origen} ) .
\end{multline*}
El hiperplano $(x_0)$ que aparece en la descomposición primaria es lo que en \cite[Sección 2]{BCJ09} llaman el ``factor extraño''. Aparece un hiperplano por cada punto base que es localmente una intersección casi completa (a.~l.~c.~i.). Siguiendo el artículo \cite{BCJ09} la ecuación del hiperplano puede ser calculada evaluando la syzygy $s_0s_2^2x_3 - s_2^2x_0$ en el punto ${(0:0:1)}$, que es la única que no se anula sobre cualquiera (en este caso el único) de los puntos base. La segunda componente es la ecuación implícita. Luego aparece una componente cuyo radical es el punto $(x_0,x_1,x_3)$.

Una descomposición del siguiente ideal de Fitting es
\begin{multline*}
\Fitt_3^1 = (x_0,x_1) \cap (x_1,x_2) \\ \cap (x_0^3,x_1^7,x_3^3,x_0x_3^2,x_0^2x_3,x_0x_1^2x_3,x_0^2x_1^2,x_1^3x_3^2,x_0x_1^4,x_1^5x_3) \\ \cap ( \text{componente soportada en el origen} ) .
\end{multline*}
Aparece la recta $(x_0,x_1)$ que está relacionada con el factor extraño. Luego aparece la recta $(x_1,x_2)$ y una componente soportada en el origen.
El siguiente ideal de Fitting es
\[ \Fitt_3^2 = (x_0^2,x_1^4,x_3^3,x_0x_3,x_1^2x_3,x_0x_1^2) \cap ( \text{componente soportada en el origen} ) . \]
El ideal de menores de cotamaño 3 es
\[ \Fitt_3^3 = (x_0,x_1^2,x_3) \cap ( \text{componente soportada en el origen} ) . \]
En estos dos últimos ideales de Fitting la única componente relevante que aparece es la asociada al punto ${(0:0:1:0)}$.
Finalmente el ideal de Fitting $\Fitt_3^4$ está soportado en el origen.

Dejando de lado el factor extraño que aparece en $\Fitt_3^0$ y $\Fitt_3^1$ en los ideales de Fitting aparecen la ecuación implícita $x_0x_1^2 + x_1^3 + x_0x_2^2$, la recta $(x_1,x_2)$ y el punto $(x_0,x_1,x_3)$.

La recta $(x_1,x_2)$ aparece únicamente en el ideal de Fitting inicial $\Fitt_3^0$ y en el siguiente $\Fitt_3^1$. Esto se debe a que dicha recta es el conjunto de puntos que la parametrización ``pinta'' exactamente 2 veces. Es análogo a lo que sucede con el Ejemplo \ref{curvas} con la curva alfa.
Observemos que si bien no aparece como componente primaria irreducible en $\Fitt_3^0$, la recta $(x_1,x_2)$ está en dicho ideal. Esto se debe a que al ser una componente embebida no hay unicidad en la descomposición primaria, cosa que si sucede para las componentes aisladas (ver Capítulo \ref{capPrelim}, Teorema \ref{teo2unic}).

Por otro lado el punto ${(0:0:1:0)}$ que aparece en los primeros 4 ideales de Fitting tiene como fibra a la recta $V(s_0)$. Al igual que como ocurre en caso de la esfera, se trata de un punto singular introducido por la parametrización.

